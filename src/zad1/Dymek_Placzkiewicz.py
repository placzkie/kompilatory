import os
import sys
import re
import codecs

#dupa dupa

def processFile(filepath):
    fp = codecs.open(filepath, 'rU', 'iso-8859-2')

    content = fp.read()

    def findInMeta(propertyName, meta) :
        pattern = r'<META NAME="' + propertyName + '" CONTENT="(.*?)">'
        r = re.compile(pattern)
        propertyMeta = r.search(content)
        if (propertyMeta):
            return propertyMeta.group(1)
        else :
            return "UNKNOWN"

    def findKeywords(meta):
        pattern = r'<META NAME="KLUCZOWE_\d*" CONTENT=(.*?)">'
        r = re.compile(pattern)
        keywords = r.findall(meta)
        if (keywords):
            return keywords
        else :
            return "UNKNOWN"

    def extractMeta(content):
        pattern = r'<META .*>'
        r = re.compile(pattern)
        metas = r.findall(content)
        return ','.join(metas)

    def extractParagraphs(content):
        pattern = r'<P>.*?<META'
        r = re.compile(pattern, re.DOTALL)
        paragraphs = r.search(content)
        paragraphsWithoutTags = re.sub(r'<.*?>', r'', paragraphs.group(0))
        if paragraphsWithoutTags :
            return paragraphsWithoutTags
        else :
            return "NO CONTENT"

    def countSentences(paragraphs):
        pattern = r'(\s([\w@\.]+[,]?\s)+[\w]{4,}[\.!\?]+\D)'
        r = re.compile(pattern, re.DOTALL)
        m = r.findall(paragraphs)
        if m :
            return m.__len__()
        else :
            return "EMPTY"

    def countShorthands(paragraphs):
        pattern = r'(\s[a-zA-Z]{1,3}\.)(?!.*\1)'
        r = re.compile(pattern, re.DOTALL)
        m = r.findall(paragraphs)
        if m :
            return m.__len__()
        else :
            return 0

    def countIntegers(paragraphs):
        pattern = r'((?:[-]?(?:' \
                  r'[0-2]?[0-9]?[0-9]?[0-9]?[0-9]|' \
                  r'3[0-1][0-9][0-9][0-9]|' \
                  r'32[0-6][0-9][0-9]|' \
                  r'327[0-5][0-9]|' \
                  r'3276[0-7])|' \
                  r'-32768)[ ,]?)(?!.*\1)'
        r = re.compile(pattern, re.DOTALL)
        m = r.findall(paragraphs)
        if m :
            return m.__len__()
        else:
            return 0

    def countDates(body):
        pattern31 = r'\s(((0[1-9])|([1-2]\d)|(3[0-1]))(?P<sep>[\.\-/])(1|3|5|7|8|10|12)(?P=sep)(\d{4}))'
        pattern30 = r'\s(((0[1-9])|([1-2]\d)|(30))(?P<sep>[\.\-/])(4|6|9|11)(?P=sep)(\d{4}))'
        pattern29 = r'\s(((0[1-9])|([1-2]\d))(?P<sep>[\.\-/])(2)(?P=sep)(\d{4}))'

        pattern31_2 = r'\s((\d{4})(?P<sep>[\.\-/])((0[1-9])|([1-2]\d)|(3[0-1]))(?P=sep)(1|3|5|7|8|10|12))'
        pattern30_2 = r'\s((\d{4})(?P<sep>[\.\-/])((0[1-9])|([1-2]\d)|(30))(?P=sep)(4|6|9|11))'
        pattern29_2 = r'\s((\d{4})(?P<sep>[\.\-/])((0[1-9])|([1-2]\d))(?P=sep)(2))'

        r31 = re.compile(pattern31)
        r30 = re.compile(pattern30)
        r29 = re.compile(pattern29)
        r29_2 = re.compile(pattern29_2)
        r30_2 = re.compile(pattern30_2)
        r31_2 = re.compile(pattern31_2)

        result = []
        result += r31.findall(body)
        result += r30.findall(body)
        result += r29.findall(body)
        result += r31_2.findall(body)
        result += r30_2.findall(body)
        result += r29_2.findall(body)
        if result:
            return set(result).__len__()
        else:
            return 0

    def countFloats(paragraphs):
        pattern = r'\s[-]?\d+[\.,]\s?\d+\s'
        r = re.compile(pattern)
        m = r.findall(paragraphs)
        if m:
            return set(m).__len__()
        else:
            return 0

    def countEmails(paragraphs):
        pattern = r'\s[a-z]+@[a-z]+(\.[a-z]+)+'
        r = re.compile(pattern)
        m = r.findall(paragraphs)
        if m:
            return set(m).__len__()
        else:
            return 0

    meta = extractMeta(content)
    paragraphs = extractParagraphs(content)

    fp.close()
    print("nazwa pliku:", filepath)
    print("autor:", findInMeta("AUTOR", meta))
    print("dzial:", findInMeta("DZIAL", meta))
    print("slowa kluczowe:", findKeywords(meta))
    print("liczba zdan:", countSentences(paragraphs))
    print("liczba skrotow:", countShorthands(paragraphs))
    print("liczba liczb calkowitych z zakresu int:", countIntegers(paragraphs))
    print("liczba liczb zmiennoprzecinkowych:", countFloats(paragraphs))
    print("liczba dat:", countDates(paragraphs))
    print("liczba adresow email:", countEmails(paragraphs))
    print("\n")



try:
    path = sys.argv[1]
except IndexError:
    print("Brak podanej nazwy katalogu")
    sys.exit(0)


tree = os.walk(path)

for root, dirs, files in tree:
    for f in files:
        if f.endswith(".html"):
            filepath = os.path.join(root, f)
            processFile(filepath)


