import AST
import SymbolTable
from Memory import *
from Exceptions import  *
from visit import *
import sys
import ast

sys.setrecursionlimit(10000)

class Interpreter(object):

    def __init__(self):
        self.operators = self.prepareOperators()

        self.globalMemory = MemoryStack()
        self.functionMemory = MemoryStack()

        self.funDefs = dict()
        self.recursionDepth = 0

    def prepareOperators(self):
        operators = {}
        operators["+"] = (lambda x, y: x + y)
        operators["-"] = (lambda x, y: x - y)
        operators["*"] = (lambda x, y: x * y)
        operators["/"] = (lambda x, y: x / y)
        operators["%"] = (lambda x, y: x % y)

        operators["<"] = (lambda x, y: x < y)
        operators[">"] = (lambda x, y: x > y)
        operators["<="] = (lambda x, y: x <= y)
        operators[">="] = (lambda x, y: x >= y)
        operators["=="] = (lambda x, y: x == y)
        operators["!="] = (lambda x, y: x != y)

        operators["&"] = (lambda x, y: x & y)
        operators["|"] = (lambda x, y: x | y)
        operators["^"] = (lambda x, y: x ^ y)
        operators[">>"] = (lambda x, y: x >> y)
        operators["<<"] = (lambda x, y: x << y)

        return operators

    @on('node')
    def visit(self, node):
        pass

    @when(AST.BinExpr)
    def visit(self, node):
        r1 = node.left.accept2(self)
        r2 = node.right.accept2(self)
        # try sth smarter than:
        # if(node.op=='+') return r1+r2
        # elsif(node.op=='-') ...
        # but do not use python eval

        if self.operators.has_key(node.operator):
            result = self.operators[node.operator](r1, r2)
        else:
            result = None
        return result

    @when(AST.RelExpr)
    def visit(self, node):
        print "visited relexpr"
        r1 = node.left.accept2(self)
        r2 = node.right.accept2(self)

        if self.operators.has_key(node.operator):
            result = self.operators[node.operator](r1, r2)
        else:
            result = None
        return result

    @when(AST.Program)
    def visit(self, node):
        node.constructs.accept2(self)

    @when(AST.Constructs)
    def visit(self, node):
        for construct in node.constructs:
            construct.accept2(self)

    @when(AST.Construct)
    def visit(self, node):
        node.construct.accept2(self)

    @when(AST.Declarations)
    def visit(self, node):
        for declaration in node.declarations:
            declaration.accept2(self)

    @when(AST.Declaration)
    def visit(self, node):
        node.inits.accept2(self)

    @when(AST.Inits)
    def visit(self, node):
        for init in node.inits:
            init.accept2(self)

    @when(AST.Init)
    def visit(self, node):
        res = node.expression.accept2(self)

        if self.recursionDepth > 0:
            self.functionMemory.insert(node.id, res)
        else:
            self.globalMemory.insert(node.id, res)
        return res

    @when(AST.Instructions)
    def visit(self, node):
        for instr in node.instructions:
            instr.accept2(self)

    @when(AST.Instruction)
    def visit(self, node):
        pass


    @when(AST.Print)
    def visit(self, node):
        res = node.expression.accept2(self)
        if res is not None:
            print res

    @when(AST.Labeled)
    def visit(self, node):
        print node.instruction.accept2(self)

    @when(AST.Assignment)
    def visit(self, node):
        res = node.expression.accept2(self)
        if self.recursionDepth > 0:
            self.functionMemory.set(node.id, res)
        else:
            self.globalMemory.set(node.id, res)
        return res

    @when(AST.Choice)
    def visit(self, node):
        node._if.accept2(self)
        if node._else is not None:
            if not node._if.cond.accept2(self):
                node._else.accept2(self)

    @when(AST.If)
    def visit(self, node):
        if node.cond.accept2(self):
            return node.statement.accept2(self)

    @when(AST.Else)
    def visit(self, node):
        node.statement.accept2(self)

    @when(AST.While)
    def visit(self, node):
        while node.cond.accept2(self):
            try:
                node.statement.accept2(self)
            except BreakException:
                break
            except ContinueException:
                pass

    @when(AST.RepeatUntil)
    def visit(self, node):
        firstCourse = True;
        while True:
            try:
                if firstCourse == True:
                    node.statement.accept2(self)
                    firstCourse = False
                    if node.cond.accept2(self):
                        break
                else:
                    if node.cond.accept2(self):
                        break
                    node.statement.accept2(self)
            except BreakException:
                break
            except ContinueException:
                pass

    @when(AST.Return)
    def visit(self, node):
        val = node.expression.accept2(self)
        raise ReturnValueException(val)


    @when(AST.Continue)
    def visit(self, node):
        raise ContinueException()

    @when(AST.Break)
    def visit(self, node):
        raise BreakException()

    @when(AST.Compound)
    def visit(self, node):
        newScope = Memory(None)
        self.globalMemory.push(newScope)
        node.declarations.accept2(self)
        node.instructions.accept2(self)
        self.globalMemory.pop()

    @when(AST.Condition)
    def visit(self, node):
        pass

    @when(AST.Expression)
    def visit(self, node):
        pass

    @when(AST.Const)
    def visit(self, node):
        pass

    @when(AST.Integer)
    def visit(self, node):
        return int(node.value)

    @when(AST.Float)
    def visit(self, node):
        return float(node.value)

    @when(AST.String)
    def visit(self, node):
        return node.value[1:len(node.value)-1]

    @when(AST.Id)
    def visit(self, node):
        funRes = self.functionMemory.get(node.id)
        if funRes is not None:
            return funRes
        else:
            res = self.globalMemory.get(node.id)
            return res

    @when(AST.ExpressionInParentheses)
    def visit(self, node):
        return node.expression.accept2(self)

    @when(AST.IdWithParentheses)
    def visit(self, node):
        fundef = self.funDefs[node.id]

        funMemory = Memory(node.id)
        for param, arg in zip(node.expression_list.expressions, fundef.arglist.arg_list):
            funMemory.put(arg.accept2(self), param.accept2(self))
        self.functionMemory.push(funMemory)
        self.recursionDepth = self.recursionDepth + 1
        try:
            fundef.compound_instr.accept2(self)
        except ReturnValueException as e:
            return e.value
        finally:
            self.recursionDepth = self.recursionDepth - 1
            self.functionMemory.pop()


    @when(AST.ExpressionList)
    def visit(self, node):
        for expr in node.expressions:
            expr.accept2(self)

    @when(AST.FunctionDefinitions)
    def visit(self, node):
        for fundef in node.fundefs:
            fundef.accept2(self)

    @when(AST.FunctionDefinition)
    def visit(self, node):
        self.funDefs[node.id] = node

    @when(AST.ArgumentList)
    def visit(self, node):
        for arg in node.arg_list:
            arg.accept2(self)

    @when(AST.Argument)
    def visit(self, node):
        return node.id
