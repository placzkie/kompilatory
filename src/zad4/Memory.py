class Memory:

    def __init__(self, name):  # memory name
        self.name = name
        self.dict = {}

    def has_key(self, name):  # variable name
        return name in self.dict

    def get(self, name):         # gets from memory current value of variable <name>
        if self.dict.has_key(name):
            return self.dict[name]
        else:
            return None

    def put(self, name, value):  # puts into memory current value of variable <name>
        self.dict[name] = value

    def printM(self):
        print "Memory content"
        for key, value in self.dict.iteritems():
            print "{0} : {1}".format(key, value)

class MemoryStack:

    def __init__(self, memory=None):  # initialize memory stack with memory <memory>
        self.stack = []
        if memory is not None:
            self.stack.append(memory)
        else:
            self.stack.append(Memory("TOP"))

    def get(self, name):             # gets from memory stack current value of variable <name>
        indices = range(len(self.stack))
        indices.reverse()
        for i in indices:
            if self.stack[i].has_key(name):
                return self.stack[i].get(name)
        return None

    def insert(self, name, value):  # inserts into memory stack variable <name> with value <value>
        self.stack[-1].put(name, value)

    def set(self, name, value):  # sets variable <name> to value <value>
        indices = range(len(self.stack))
        indices.reverse()
        for i in indices:
            if self.stack[i].has_key(name):
                self.stack[i].put(name, value)
                break

    def push(self, memory):  # pushes memory <memory> onto the stack
        self.stack.append(memory)

    def pop(self):          # pops the top memory from the stack
        self.stack.pop()

    def printMS(self):
        for entry in self.stack:
            entry.printM()