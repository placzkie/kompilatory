class Symbol(object):
    def __init__(self, name, type):
        self.name = name
        self.type = type

class VariableSymbol(Symbol):
    def __init__(self, name, type):
        super(VariableSymbol, self).__init__(name, type)


class FunctionSymbol(Symbol):
    def __init__(self, name, type, params):
        super(FunctionSymbol, self).__init__(name, type)
        self.params = params

class SymbolTable(object):
    def __init__(self, parent, name, type):
        self.symbol = Symbol(name, type)
        self.parent = parent
        self.symbols = {}
        self.args = {}

    def put(self, name, symbol): # put variable symbol or fundef under <name> entry
        self.symbols[name] = symbol

    def get(self, name): # get variable symbol or fundef from <name> entry
        return self.symbols[name]

    def get_parent_scope(self):
        return self.parent

    def putArgsAmount(self, name, amount):
        self.args[name] = amount

    def getArgsAmount(self, name):
        if self.args is not None:
            if self.args.has_key(name):
                return self.args[name]
        elif self.get_parent_scope() is not None:
            return self.get_parent_scope().getArgsAmount(name)

    def getArgsCount(self, name):
        for key, val in self.symbols.iteritems():
            if val.name == name:
                if isinstance(val, FunctionSymbol):
                    return (len(val.params.arg_list))
        return self.get_parent_scope().getArgsCount(name)

    def getArgType(self, name, index):
        if name in self.symbols:
            symbol = self.get(name)
            return symbol.params.arg_list[index].type
        else:
            return self.get_parent_scope().getArgType(name, index)

    def getRetType(self, name):
        if name in self.symbols:
            symbol = self.get(name)
            return symbol.type
        else:
            return self.get_parent_scope().getRetType(name)

    def printC(self, indent):
        print "\t" * indent + "Tablica:"
        for x in self.symbols:
            print "\t" * indent + x
        print "Parent:"
        if self.get_parent_scope() is not None:
            self.get_parent_scope().printC(indent + 1)

        for key, val in self.symbols.iteritems():
            if isinstance(self.symbols[key], FunctionSymbol):
                print '{0}:{1}'.format(self.symbols[key].name, len(self.symbols[key].params.arg_list))