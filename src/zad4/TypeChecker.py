#!/usr/bin/python

from SymbolTable import SymbolTable, VariableSymbol, FunctionSymbol, Symbol
import AST


ttype = {}
arithmetic_operators = ['+', '-', '*', '/', '%']
bitwise_operators = ['|', '&', '^', '<<', '>>']
logical_operators = ['&&', '||']
comparison_operators = ['==', '!=', '>', '<', '<=', '>=']
assignment_operators = ['=']


def all_operators():
    return arithmetic_operators + bitwise_operators + logical_operators + assignment_operators + comparison_operators


for operator in all_operators():
    ttype[operator] = {}
    for type_ in ['int', 'float', 'string']:
        ttype[operator][type_] = {}

for arithmetic_operator in arithmetic_operators:
    ttype[arithmetic_operator]['int']['int'] = 'int'
    ttype[arithmetic_operator]['int']['float'] = 'float'
    ttype[arithmetic_operator]['float']['int'] = 'float'
    ttype[arithmetic_operator]['float']['float'] = 'float'
ttype['+']['string']['string'] = 'string'
ttype['*']['string']['int'] = 'string'
ttype['=']['float']['int'] = 'float'
ttype['=']['float']['float'] = 'float'
ttype['=']['int']['int'] = 'int'
ttype['=']['string']['string'] = 'string'
ttype['=']['float']['int'] = 'float'

for operator in bitwise_operators + logical_operators:
    ttype[operator]['int']['int'] = 'int'

for comp_op in comparison_operators:
    ttype[comp_op]['int']['int'] = 'int'
    ttype[comp_op]['int']['float'] = 'int'
    ttype[comp_op]['float']['int'] = 'int'
    ttype[comp_op]['float']['float'] = 'int'
    ttype[comp_op]['string']['string'] = 'int'

class NodeVisitor(object):

    def visit(self, node):
        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)


    def generic_visit(self, node):        # Called if no explicit visitor function exists for a node.
        if isinstance(node, list):
            for elem in node:
                self.visit(elem)
        else:
            for child in node.children:
                if isinstance(child, list):
                    for item in child:
                        if isinstance(item, AST.Node):
                            self.visit(item)
                elif isinstance(child, AST.Node):
                    self.visit(child)

    # simpler version of generic_visit, not so general
    #def generic_visit(self, node):
    #    for child in node.children:
    #        self.visit(child)

class TypeChecker(NodeVisitor):

    def __init__(self):
        self.tab = SymbolTable(None, "Program", None)
        self.loopDepth = 0
        self.insideFunction = False
        self.current_fun = None
        self.funcallExpr = False
        self.returned = False
        self.invoked_fun = None
        self.argIdx = 0
        self.isValid = True

    def findVariable(self, tab, variable):
        if tab is None:
            return None
        if variable in tab.symbols:
            return tab.get(variable)
        elif tab.symbol.name == variable:
            return tab.symbol
        elif tab.get_parent_scope() is not None:
            return self.findVariable(tab.get_parent_scope(), variable)

        return None


    def visit_Program(self, node):
        pass

    def visit_Constructs(self, node):
        for construct in node.constructs:
            self.visit(construct)

    def visit_Construct(self, node):
        self.visit(node.construct)

    def visit_Declarations(self, node):
        for declaration in node.declarations:
            self.visit(declaration)

    def visit_Declaration(self, node):
        self.current_type = node.type
        self.visit(node.inits)

    def visit_Inits(self, node):
        for init in node.inits:
            self.visit(init)

    def visit_Init(self, node):
        symbol = self.findVariable(self.tab, node.id)
        if node.id in self.tab.symbols:
            self.isValid = False
            print "Error: Variable '{0}' already declared: line {1}".format(node.id, node.line)
        elif symbol is not None:
            if self.tab.symbol.name == node.id:
                self.isValid = False
                print "Error: Function identifier '{0}' used as a variable: line {1}".format(node.id, node.line)
        else:
            value_type = self.visit(node.expression)
            if not value_type in ttype["="][self.current_type]:
                self.isValid = False
                print "Error: Assignment of {0} to {1}: line {2}".format(value_type, self.current_type, node.line)
            else:
                self.tab.put(node.id, VariableSymbol(node.id, self.current_type))

    def visit_Instructions(self, node):
        for instruction in node.instructions:
            self.visit(instruction)

    def visit_Instruction(self, node):
        pass

    def visit_Print(self, node):
        self.visit(node.expression)

    def visit_Labeled(self, node):
        self.visit(node.instruction)

    def visit_Assignment(self, node):
        variable = self.findVariable(self.tab, node.id)
        if variable is None:
            self.isValid = False
            print "Error: Variable '{0}' undefined in current scope: line {1}".format(node.id, node.line)
        else:
            value_type = self.visit(node.expression)
            if value_type is None:
                return
            if not value_type in ttype["="][variable.type]:
                self.isValid = False
                print ("Value of type {0} cannot be assigned to symbol {1} of type {2} (line {3})" \
                       .format(value_type, node.id, variable.type, node.line - 1))
            else:
                return ttype["="][variable.type][value_type]

    def visit_Choice(self, node):
        self.visit(node._if)
        if node._else is not None:
            self.visit(node._else)

    def visit_If(self, node):
        self.visit(node.cond)
        self.visit(node.statement)

    def visit_Else(self, node):
        self.visit(node.statement)

    def visit_While(self, node):
        self.loopDepth = self.loopDepth + 1
        self.visit(node.cond)
        self.visit(node.statement)
        self.loopDepth = self.loopDepth - 1

    def visit_RepeatUntil(self, node):
        self.loopDepth = self.loopDepth + 1
        self.visit(node.cond)
        self.visit(node.statement)
        self.loopDepth = self.loopDepth - 1

    def visit_Return(self, node):
        actualType = self.visit(node.expression)
        expectedType = self.tab.getRetType(self.current_fun)

        if actualType is None:
           pass
        elif actualType != expectedType:
            self.isValid = False
            print "Error: Improper returned type, expected {0}, got {1}: line {2}".format(expectedType, actualType, node.line)
        elif self.insideFunction == False:
            self.isValid = False
            print 'Error: return instruction outside a function: line {0}'.format(node.line)
        self.returned = True

    def visit_Continue(self, node):
        if self.loopDepth <= 0:
            self.isValid = False
            print 'Error: continue instruction outside a loop: line {0}'.format(node.line)

        pass

    def visit_Break(self, node):
        if self.loopDepth <= 0:
            self.isValid = False
            print 'Error: break instruction outside a loop: line {0}'.format(node.line)
        pass

    def visit_Compound(self, node):
        if not self.insideFunction:
            self.tab = SymbolTable(self.tab, None, None)
        self.visit(node.declarations)
        self.visit(node.instructions)
        if not self.insideFunction:
            self.tab = self.tab.get_parent_scope()

    def visit_Condition(self, node):
        pass

    def visit_Expression(self, node):
        pass

    def visit_Const(self, node):
        pass

    def visit_Integer(self, node):
        return 'int'

    def visit_String(self, node):
        return 'string'

    def visit_Float(self, node):
        return 'float'

    def visit_Id(self, node):
        variable = self.findVariable(self.tab, node.id)
        if variable is None:
            self.isValid = False
            print "Error: Usage of undeclared variable '{0}': line {1}".format(node.id, node.line)
        else:
            return variable.type

    def visit_BinExpr(self, node):
        type1 = self.visit(node.left)
        type2 = self.visit(node.right)
        operator = node.operator

        if type1 is None or not type2 in ttype[operator][type1]:
            self.isValid = False
            print "Error: Illegal operation, {0} {1} {2}: line {3}".format(type1, operator, type2, node.line)
        else:
            return ttype[operator][type1][type2]

    def visit_RelExpr(self, node):
        type1 = self.visit(node.left)
        type2 = self.visit(node.right)
        comp_op = node.op

        if type1 is None or not type2 in ttype[comp_op][type1]:
            print ("Incompatible types in line", node.line)
        else:
            return ttype[comp_op][type1][type2]

    def visit_ExpressionInParentheses(self, node):
        expression = node.expression
        return self.visit(expression)

    def visit_IdWithParentheses(self, node):
        self.argIdx = 0
        variable = self.findVariable(self.tab, node.id)
        self.invoked_fun = node.id
        if variable is None:
            self.isValid = False
            print "Error: Call of undefined fun: '{0}': line {1}".format(node.id, node.line)
        else:
            self.visit(node.expression_list)
            return variable.type



    def visit_ExpressionList(self, node):
        expectedCount = self.tab.getArgsCount(self.invoked_fun)
        actualCount = len(node.expressions)
        if actualCount != expectedCount:
            self.isValid = False
            print "Error: Improper number of args in {0} call: line {1}".format(self.invoked_fun, node.line)
        else:
            for arg in node.expressions:
                expectedType = self.tab.getArgType(self.invoked_fun, self.argIdx)
                self.argIdx = self.argIdx + 1
                actualType = self.visit(arg)
                if actualType != expectedType:
                    print "Error: Improper type of args in {0} call: line {1}".format(self.invoked_fun, node.line)
                    break

    def visit_FunctionDefinitions(self, node):
        for fundef in node.fundefs:
            self.visit(fundef)

    def visit_FunctionDefinition(self, node):
        if self.findVariable(self.tab, node.id) is not None:
            self.isValid = False
            print "Error: Redefinition of function '{0}': line {1}".format(node.id, node.line)
        else:
            self.insideFunction = True
            self.tab.put(node.id, FunctionSymbol(node.id, node.type, node.arglist))
            self.current_fun = node.id
            self.tab = SymbolTable(self.tab, node.id, node.type)
            self.returned = False
            self.visit(node.arglist)
            self.visit(node.compound_instr)
            if not self.returned:
                self.isValid = False
                print "Error: Missing return statement in function '{0}' returning {1}: line {2}".format(self.current_fun, self.tab.symbol.type, node.line)
            self.tab = self.tab.get_parent_scope()
            self.insideFunction = False

    def visit_ArgumentList(self, node):
        self.tab.putArgsAmount(self.current_fun, len(node.arg_list))
        for arg in node.arg_list:
            self.visit(arg)

    def visit_Argument(self, node):
        self.tab.put(node.id, VariableSymbol(node.id, node.type))